package com.ceadeal.javafxjdk11start;

import com.ceadeal.javafxjdk11start.config.AppConfig;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;

public class App extends Application {

    public static void main(String[] args) {
        AppConfig.init();
        launch();
    }

    @Override
    public void start(Stage stage) throws IOException {
//        log.info("start");
        // 加载并创建主场景
        Parent root = FXMLLoader.load(App.class.getResource("fxml/Main.fxml"));
        // root.getStylesheets().add(ResourceUtil.getResource("css/Main.css").toExternalForm());
        Scene scene = new Scene(root, AppConfig.stageWidth, AppConfig.stageHeight);
        // 设置窗口信息
        stage.setTitle(AppConfig.title);
        stage.setResizable(AppConfig.stageResizable);
        stage.getIcons().add(new Image(App.class.getResourceAsStream(AppConfig.icon)));
        stage.setScene(scene);
        stage.show();
    }

}
