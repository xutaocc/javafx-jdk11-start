module javafx.jdk11.start {
    requires javafx.controls;
    requires javafx.fxml;

    opens com.ceadeal.javafxjdk11start.ctrl to javafx.fxml;

    exports com.ceadeal.javafxjdk11start;
}